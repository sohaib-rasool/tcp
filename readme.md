### Demo TCP implementation 

`main.cpp` make an object of `TcpClient` class which is already implemented and communicates with an existing __TCP Server__ on port 8080 of local machine.

#### epoll.cpp

Takes an IP and two PORTS. 
Makes two TCP clients which are connected to servers on each port.

Epoll waits for:
* Server 1 and echo it back. (shows timeout message if nothing received from echo seerver for last 10 seonds)
* STDIN(console) and send it to server 2