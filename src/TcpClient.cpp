#include "TcpClient.h"

#include <errno.h>
TcpClient::TcpClient()
{
   sock = 0;
   memset(buffer, 0, sizeof(buffer));
};

void TcpClient::set_tcp_ip_and_port(std ::string ip, int port)
{

   _ip   = ip;
   _port = port;
};
int TcpClient::get_sock()
{
   return sock;
};
bool TcpClient ::connect()
{

   if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
   {
      throw errno;
      return 1;
   }

   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port   = htons(_port);

   // Convert IPv4 and IPv6 addresses from text to binary form
   int ip_text2bin = inet_pton(AF_INET, _ip.c_str(), &serv_addr.sin_addr);

   if (ip_text2bin < 0)
   {
      throw errno;
      return 1;
   }
   else if (ip_text2bin == 0)
   {
      throw "Invlaid IP/PORT";
      return 1;
   }

   if (::connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
   {
      throw errno;
      return 1;
   }
   return 0;
};

bool TcpClient::send(char *input_data)
{

   if (::send(sock, input_data, strlen(input_data), 0) != -1)

   {
      std::cout << "Sent to Server: " << _ip << ":" << _port << std::endl;
      std::cout << input_data << std::endl;
      std::cout << "---------------------------------------" << std::endl;
      return 0;
   }
   else
   {
      return 1;
   }
};

void TcpClient::continuous_receive()
{

   while (1)
   {

      valread = read(sock, buffer, 1024);
      std::cout << "From Server:" << std::endl;
      printf("%s\n", buffer);
      send(buffer);

      memset(buffer, 0, sizeof(buffer));  // reset buffer
   }
};

// int TcpClient::start()
// {
//  int                sock = 0, valread;
//  struct sockaddr_in serv_addr;
//  char *             hello        = "TcpCLient started on given PORT \n";
//  char               buffer[1024] = {0};
//  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
//  {
// printf("\n Socket creation error \n");
// return -1;
//  }
//
//  serv_addr.sin_family = AF_INET;
//  serv_addr.sin_port   = htons(PORT);
//
// Convert IPv4 and IPv6 addresses from text to binary form
//  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
//  {
// printf("\nInvalid address/ Address not supported \n");
// return -1;
//  }
//
//  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
//  {
// printf("\nConnection Failed \n");
// return -1;
//  }
//
//  send(sock, hello, strlen(hello), 0);
//  printf(" START message sent\n");
//  valread = read(sock, buffer, 1024);
//  printf("%s\n", buffer);
//  return 0;
// }
