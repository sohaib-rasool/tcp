#include "timer.h"

#include <errno.h>

int timer::timer_create()
{
   timer_fd = timerfd_create(CLOCK_REALTIME, 0);
   if (timer_fd < 0)
   {
      throw errno;
   }
   return timer_fd;
};

int timer::timer_set(int initital_value, int interval)
{
   if (clock_gettime(CLOCK_REALTIME, &now) == -1)
   {
      throw errno;
   }
   timeout.it_value.tv_sec     = now.tv_sec + initital_value;  // realtime plus 10 sec counter
   timeout.it_value.tv_nsec    = now.tv_nsec;
   timeout.it_interval.tv_sec  = interval;  // 10 sec interval after first expiration
   timeout.it_interval.tv_nsec = 0;
   // setting timer
   if (timerfd_settime(timer_fd, TFD_TIMER_ABSTIME, &timeout, NULL) == -1)
   {

      throw errno;
   }
   return 0;
};

int timer::get_fd()
{
   return timer_fd;
};