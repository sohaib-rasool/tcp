#ifndef EPOLL_H
#define EPOLL_H
#include <sys/epoll.h>
#include <unistd.h>
#define MAX_EVENTS 5
class epoll
{

 private:
 public:
   struct epoll_event event, events[MAX_EVENTS];
   int                epoll_fd;
   epoll();

   int epoll_add(int fd);
};

#endif