#ifndef TIMER_H
#define TIMER_H
#include <sys/timerfd.h>

class timer
{

 private:
   struct itimerspec timeout;
   struct timespec   now;
   int               timer_fd;

 public:
   int get_fd();
   int timer_create();
   int timer_set(int initital_value, int interval);
};
#endif