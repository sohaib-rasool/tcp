#ifndef TCPCLIENT_H
#define TCPCLIENT_H
#include <arpa/inet.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
class TcpClient
{

 private:
   std::string        _ip;
   int                _port;
   int                sock, valread;
   struct sockaddr_in serv_addr;

   char buffer[1024];

 public:
   TcpClient();
   int  start();
   void set_tcp_ip_and_port(std ::string ip, int port);
   bool connect();
   bool send(char *input_data);
   void continuous_receive();
   int  get_sock();
};

#endif