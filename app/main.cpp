#include "TcpClient.h"

#include <cmath>
using namespace std;

int main(int argc, char **argv)
{
   if (argc != 3)
   {
      cout << "Please Enter correct number of arguments \n";
      cout << "Usage : ./client ip port" << endl;
      return 0;
   }

   TcpClient client;

   try
   {
      client.set_tcp_ip_and_port(argv[1], stoi(argv[2]));
      client.connect();
      client.continuous_receive();
   }
   catch (char const *message)
   {
      cout << "EXCEPTION:" << message << endl;
   }
   catch (int error)
   {

      cout << "Errno: \n" << strerror(error) << endl;
   }

   return 0;
};