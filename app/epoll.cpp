#define READ_SIZE 10
#include "epoll.h"

#include "TcpClient.h"
#include "timer.h"

#include <string.h>  // for strncmp
#include <unistd.h>  // for close(), read()
using namespace std;

int main(int argc, char **argv)
{
   if (argc != 4)  // checking for input arguments
   {
      cout << "Please Enter correct number of arguments \n";
      cout << "Usage : ./epoll ip port1(echo) port2(console_to_server)" << endl;
      return 0;
   }

   int    running = 1, event_count, i;
   size_t bytes_read;
   char   read_buffer[READ_SIZE + 1];

   TcpClient tcp;       // client to send data from console
   TcpClient tcp_echo;  // client to echo data from server
   epoll     epoll;
   timer     timer;

   enum fd_index
   {
      console_fd,   // 0
      tcp_echo_fd,  // 1
      timer_fd,     // 2
      total_fd = 3  // total number of fd enums

   };
   int fd[total_fd] = {0};  // array to store all fd in use (initially set to zero)

   try
   {
      // initialising sockets
      tcp_echo.set_tcp_ip_and_port(argv[1], stoi(argv[2]));
      tcp_echo.connect();
      fd[tcp_echo_fd] = tcp_echo.get_sock();  // add fd to our fd array

      tcp.set_tcp_ip_and_port(argv[1], stoi(argv[3]));
      tcp.connect();

      // add timer
      timer.timer_create();
      timer.timer_set(10, 10);
      fd[timer_fd]   = timer.get_fd();  // add fd to our fd array
      fd[console_fd] = STDIN_FILENO;    // add fd to our fd array

      // adding fd to epoll
      epoll.epoll_add(fd[console_fd]);
      epoll.epoll_add(fd[tcp_echo_fd]);
      epoll.epoll_add(fd[timer_fd]);
   }
   catch (char const *message)
   {
      cout << "EXCEPTION:" << message << endl;
      exit(0);
   }
   catch (int error)
   {
      cout << "Errno: \n" << strerror(error) << endl;
      exit(0);
   }
   while (running)
   {
      event_count = epoll_wait(epoll.epoll_fd, epoll.events, MAX_EVENTS, 0);  // check for any ready event

      for (i = 0; i < event_count; i++)  // iterating over all ready events
      {

         bytes_read =
            read(epoll.events[i].data.fd, read_buffer, READ_SIZE);  // read data from either console fd==0 or tcp_echo
         read_buffer[bytes_read] = '\0';                            // clear unused buffer
         if (epoll.events[i].data.fd == fd[console_fd])             // checking which event
         {
            tcp.send(read_buffer);  // sending data to respective server
         }
         else if (epoll.events[i].data.fd == fd[tcp_echo_fd])
         {
            tcp_echo.send(read_buffer);  // sending data to respective server
            timer.timer_set(10, 10);     // reset timer
            cout << "Timer reset\n---------------------------------------" << endl;
         }
         else if (epoll.events[i].data.fd == fd[timer_fd])
         {
            cout << "Timeout: Echo Client did not receive anything for last 10 sec!" << endl;
            ;
         }
         if (!strncmp(read_buffer, "stop\n", 5))  // stopping the code
            running = 0;
      }
   }
   if (close(epoll.epoll_fd))  // closing epoll
   {
      fprintf(stderr, "Failed to close epoll file descriptor\n");
      return 1;
   }
   if (close(fd[timer_fd]))  // closing timer
   {
      fprintf(stderr, "Failed to close timer file descriptor\n");
      return 1;
   }
   return 0;
}
